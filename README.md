
# Axional Resources
---

This repository contains the files used in docs.deistercloud.com and since 2024 it can be used
as resource provider for Airtool Studio sample applications and documentation.

### Referencing files from this repository

To reference a file in this repository

[https://bitbucket.org/deister/axional-docs-resources/raw/master/README.md] (This README file)

### Other data sources

[https://www.kaggle.com/] (Kaggle)

[https://databank.worldbank.org/data/databases] (World Bank)

[https://www.dataquest.io/blog/free-datasets-for-projects] (Data quest)