<?xml version="1.0"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xslt"
                xmlns:chart="MyChart"
                version="1.0">
<!-- 

Scaling
Currently, SVG images are rendered with the dimensions specified in the SVG file, within the viewport 
specified in the fo:external-graphic element. For everything to work properly, the two should be equal. 
The SVG standard leaves this issue as an implementation detail. Additional scaling options are available 
through XSL-FO means.

If you use pixels to specify the size of an SVG graphic the "source resolution" setting in the configuration 
will be used to determine the size of a pixel. The use of pixels to specify sizes is discouraged as they 
may be interpreted differently in different environments.

 -->                
  <xalan:component prefix="chart" elements="" functions="getCardioSVG">
  	
  	<!-- 
  	
  	 Script imported here gives no problem as it's resolved automatically.
  	 Script imported via: 
  	 
  	<xalan:component prefix="chart">
		<xalan:script lang="javascript" src="graphics-lib.js" />
	</xalan:component>
	
	will be problematic to resolve. Xalan will tru to resolve URL relative
	to current path (or using StreamSource systemid).
	An option could be provide an absolute URL like to file or http.
	Or even provide a custom protocol resolver.
  	 
  	 -->
    <xalan:script lang="javascript">
<![CDATA[

    var FONT            = "FormataProLight";
	var NS 				= "http://www.w3.org/2000/svg";
	var WIDTH   		= 800;
	var HEIGHT  		= 600;
	var COLORS_GREEN  	=  ["#e1e4de","#cfcec0","#9fb2a2","#759c70","#337a4b"];  //["#A6DEBE","#6AC893","#4CBD7D","#208950","#1B7443"];
	var COLORS_BLUE 	=  [/*'#c4eaf9','#87d5f2','#49bfeb',*/'#1D92ED', '#2088c2', '#1070ac','#0d5c98'];;
	var MARGIN_LEFT 	=  10;
	var MARGIN_RIGHT 	=  10;
	var MARGIN_TOP 		=  40;
	var MARGIN_BOTTOM 	=  50;


	function SVGElement(tag) {
		this.tag		 = tag;
		this.attributtes = {};
		this.children    = [];
		this.content     = null;
	}

	SVGElement.prototype.setAttributeNS = function(ns, key, value) {
		this.attributtes[key] = value;
	}


	SVGElement.prototype.appendChild = function(svgChild) {
		this.children.push(svgChild);
	}

	SVGElement.prototype.toSVGText = function() {
		var str = [];

		// opening tag
		str.push("<svg:" + this.tag + " ");

		// attributtes
	   	for (var key in this.attributtes) {
	     	if(this.attributtes.hasOwnProperty(key)){
	       		str.push(" " + key + "= \"" + this.attributtes[key] + "\" ");
	      	}
	   	}
	  	str.push(">");

	  	// add children
	  	for (var idx = 0; idx < this.children.length; idx++) {
	  		if (this.children[idx] instanceof SVGElement)
	  			str.push(this.children[idx].toSVGText());
	  		else
	  			str.push(this.children[idx]);
	  	}

	  	// closing tag
	  	str.push("</svg:" + this.tag + ">");


	  	return str.join("");
	}

	function SVG(){
 		var svg= new SVGElement("svg");
	
 		return svg;
	}
	
	function rect(x, y, w, h, fill) {		
	 	var SVGObj= new SVGElement("rect");
        SVGObj.setAttributeNS(null, 'x', x);
        SVGObj.setAttributeNS(null, 'y', y);
        SVGObj.setAttributeNS(null, 'width', w);
	 	SVGObj.setAttributeNS(null, "height",h);
	 	SVGObj.setAttributeNS(null, "fill", fill);
	 	return SVGObj;
	}


	function addText(svg, x, y, text, size, color, anchor, valign) {
		var SVGObj= new SVGElement("text");
        SVGObj.setAttributeNS(null, 'x', x);
        SVGObj.setAttributeNS(null, 'y', y);	        
		SVGObj.setAttributeNS(null,"font-size",size);	 
		SVGObj.setAttributeNS(null,"font-family", FONT);	 
		//SVGObj.setAttributeNS(null,"font-weight","bold");	
		SVGObj.setAttributeNS(null,"fill",color);	 

		if (valign)
			SVGObj.setAttributeNS(null,"alignment-baseline",valign);	 

		if (anchor)
			SVGObj.setAttributeNS(null,"text-anchor",anchor);	 

		var idx = text.indexOf("\n");

		if (idx == -1) {
			SVGObj.appendChild(text)
			svg.appendChild(SVGObj);	
		} else {
			var lines = text.split("\n");
			for(var row = 0; row<lines.length; row++) {
				var tspan= new SVGElement("tspan");
				tspan.setAttributeNS(null,"x",x);	
				tspan.setAttributeNS(null,"dy", (row*1.2) + "em");	
				tspan.appendChild(lines[row]);
				SVGObj.appendChild(tspan)
			}
			svg.appendChild(SVGObj);	
		}
	}

	/**
	*
	*/

	function addRow(root, x, y, rect_width, height, data) {
		data 		= toPercents(data);
		var delta 	=  15;
		var radius 	=  20;
		var values 	= data.pixel_values;
		var labels  = data.labels;
		var title   = data.title;
		var myvalue = data.myvalue;
		var valuesLabels = data.valuesLabels;
		var height  = height - MARGIN_BOTTOM - MARGIN_TOP;
		var max  	= values[values.length -1];

		var rec_spacing = 2;
		var startX 		= x;

		// title
		addText(root, x, y + MARGIN_TOP + -15,  data.title, 14, "#333333", anchor);

		// total row width
		var total_width = rect_width-delta;
		for (var idx = 0; idx < values.length; idx++) {

			var anchor = (idx == (values.length -1))? "end" : "start";
			// axis labels
			addText(root, x, y + MARGIN_TOP + height +MARGIN_BOTTOM*.75, labels[idx], 12, "#333333", anchor);
			addText(root, x, y + MARGIN_TOP + height +MARGIN_BOTTOM*.75/2,  valuesLabels[idx], 12, "#888888", anchor);

			var color_shift = data.palette.length - values.length +1;
			var last_color;
			if (idx <= values.length-2) {
				var width 	= total_width*(values[idx+1] -values[idx]);
				last_color	= data.palette[idx + color_shift];
				var r 		= rect(x, y + MARGIN_TOP, width-rec_spacing, height, last_color);
	
				root.appendChild(r);
				x 			= x + width;
			}
		}

		// add end trianle		
		x =  x - rec_spacing;
		var polygon = new SVGElement("polygon");
		var points  = x + ","  + (y + MARGIN_TOP) + "  " + (x + delta) + "," + (y + MARGIN_TOP + height /2) + "  " + x + "," +(y + MARGIN_TOP + height);
		polygon.setAttributeNS(null,"points",points);
		polygon.setAttributeNS(null,"fill", last_color);
		root.appendChild(polygon);

		//<ellipse cx="200" cy="80" rx="100" ry="50"
		var circle = new SVGElement("ellipse"); 
		circle.setAttributeNS(null,"cx", startX + data.myvalue* total_width);
		circle.setAttributeNS(null,"cy", y + MARGIN_TOP);
		circle.setAttributeNS(null,"rx", radius-1);
		circle.setAttributeNS(null,"ry", radius+1);
		circle.setAttributeNS(null,"stroke", "white");
		circle.setAttributeNS(null,"stroke-width", 3);
		circle.setAttributeNS(null,"fill", "#E34234");
		root.appendChild(circle);

		var str  = data.myvalueLabel;
		var textX = startX + data.myvalue* total_width;
		addText(root, textX, y + MARGIN_TOP, str, 18, "#FFFFFF", "middle", "baseline");
	}

	var toPercents= function(data) {
		var values   =  data.values;
		var total    = values[values.length -1];
		data.myvalue = data.myvalue/total;
		var pixel_values = [];
		for(var idx = 0; idx < values.length; idx++) {
			pixel_values.push(values[idx]/total)
		}

		data.pixel_values =  pixel_values;

		return data;
	}


	var data = [
	{
		leftMargin : 0,
		heightRatio: 1,
		palette: COLORS_BLUE,
		title : 'CRP level test',
		values: [0,1,3,10],
		valuesLabels: ["mg/L", "1", "3","+10"],
		labels: ["Low risk", "Average risk", " High risk of cardiovascular disease",""]
	},
	{
		leftMargin : 0,
		heightRatio: 1,
		palette: COLORS_GREEN,
		title : 'Total cholesterol level',
		values: [0,200,240, 280],
		valuesLabels: ["mg/L", "200", "240","+280"],
		labels: ["Desirable", "Borderline", "High", ""]
	},
	{
		leftMargin : 50,
		heightRatio: 0.85,
		palette: COLORS_GREEN,
		title : 'LDL("bad" cholesterol)',
		values: [0,100,130, 160, 190, 260],
		valuesLabels: ["mg/L","100","130", "160", "190", "+260"],	
		labels: ["Optimal", "Near\nOptimal", "Bordeline\nHigh", "High", "Very high", ""]
	},
	{
		leftMargin : 50,
		heightRatio: 0.85,
		palette: COLORS_GREEN,
		title : 'HDL("good" cholesterol)',
		values: [0,40,60, 100],
		valuesLabels:  ["mg/L","40","60", "+100"],
		labels: ["Low", "Borderline", "Optimal", ""]
	}
	];


	/**
	* 
	*
	*
	*
	*/
	function getCardioSVG(width_cm, height_cm, cpr, total_cholesterol, ldl, hdl) 
	{
		// arguments to array, copy values to data
		var args = Array.prototype.slice.call(arguments,2);
		var current_data = [];
		for(var idx = 0; idx < data.length; idx++) {
			data[idx].myvalue = args[idx];
			data[idx].myvalueLabel = "" + args[idx];				
		}

		var svgEl 		= SVG();

		svgEl.setAttributeNS(null, "width",   width_cm  + "cm");
		svgEl.setAttributeNS(null, "height",  height_cm + "cm");
		svgEl.setAttributeNS(null, "viewBox", "0 0 " + (WIDTH +100) + "  " + HEIGHT);


		// width="45px" height="135px" viewBox="0 0 90 90"

		var height_sum = 0
		for( var idx = 0; idx < data.length; idx++) {
			var rowHeight 	= HEIGHT*data[idx].heightRatio/data.length;
			addRow(svgEl, MARGIN_LEFT + data[idx].leftMargin /* x */,  height_sum /* y*/ , WIDTH - data[idx].leftMargin /*  width */, rowHeight /* height */, data[idx] /* the data */);
			height_sum = height_sum + rowHeight;
		} 

		return svgEl.toSVGText();

	}	
 
]]>
    </xalan:script>
  </xalan:component>
</xsl:stylesheet>