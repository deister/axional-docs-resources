<?xml version="1.0" encoding="UTF-8"?>
<!-- 
SAMPLE XSL FILE TO PRODUCE A LABCO "Blood Work Cardiology Result" PDF
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:chart="MyChart" extension-element-prefixes="chart">
 <!-- Include the Javascript generator of Blood Work Cardiology chart -->
 <!-- The XSLT is run via XSQLScript tag xslt.process including a URI resolver 
      that transforms chart.xsl into blood_chart.xsl as sample of how to load an external resource
 <xsl:import href="chart.xsl" />
  -->
 <xsl:import href="blood_chart.xsl" />
 <!-- start XSLT -->
 <xsl:output method="xml" indent="yes" />
 <!-- A template to draw the dotted line separator -->
 <xsl:template name='dotted-separator'>
  <fo:table-cell padding-left='0.5cm' padding-top='5.0pt' padding-bottom='5.0pt'>
   <fo:table table-layout='fixed' width='100%'>
    <fo:table-column column-width='0.0cm' />
    <fo:table-column column-width='18.0cm' />
    <fo:table-column column-width='0.0cm' />
    <fo:table-body>
     <fo:table-row>
      <fo:table-cell>
       <fo:block />
      </fo:table-cell>
      <fo:table-cell border-bottom-style='dotted'>
       <fo:block />
      </fo:table-cell>
      <fo:table-cell>
       <fo:block />
      </fo:table-cell>
     </fo:table-row>
    </fo:table-body>
   </fo:table>
  </fo:table-cell>
 </xsl:template>
 <!-- foreach XML ROW -->
 <xsl:template match="/">
  <!-- debug variable to see borders -->
  <xsl:variable name='border_width'>
   <xsl:choose>
    <xsl:when test='labco-report/data/debug = 1'>
     0.5pt
    </xsl:when>
    <xsl:otherwise>
     0pt
    </xsl:otherwise>
   </xsl:choose>
  </xsl:variable>
  <!-- FOP start -->
  <fo:root xmlns:fo='http://www.w3.org/1999/XSL/Format'>
   <!-- FOP master page layout -->
   <fo:layout-master-set>
    <fo:simple-page-master master-name='Portrait' page-width='21.0cm' page-height='29.7cm' margin-top='0.5cm' margin-bottom='0.5cm' margin-left='0.5cm' margin-right='0.5cm'>
     <fo:region-body region-name='main' margin-bottom='0.5cm' margin-top='0.5cm' margin-left='0.5cm' margin-right='0.5cm' background-color="#f1f1f1" />
     <fo:region-before extent='0.25cm' precedence='true' />
     <fo:region-after extent='0.25cm' />
    </fo:simple-page-master>
   </fo:layout-master-set>
   <!-- BEGIN PAGE SEQUENCE -->
   <fo:page-sequence force-page-count='no-force' master-reference='Portrait' initial-page-number='1'>
    <fo:flow flow-name='main'>
     <xsl:for-each select='labco-report/data/request'>
      <fo:block page-break-before="always" />
      <fo:table font-size='10pt' font-family='FormataProLight' table-layout='fixed' width='100%'>
       <fo:table-column column-width='19.0cm' />
       <fo:table-body>
        <fo:table-row>
         <!-- BACKGROUND COLOR CELL BEGIN -->
         <fo:table-cell border-style='solid' border-width='{$border_width}'>
          <!-- TAULA NECESSARIA PER EL BORDER I MARGES -->
          <fo:table table-layout='fixed' width='100%'>
           <fo:table-column column-width='19.0cm' />
           <fo:table-body>
            <!-- ROW 0 -->
            <fo:table-row>
             <fo:table-cell padding-left='0.5cm' padding-top='0.5cm' border-style='solid' border-width='{$border_width}'>
              <fo:table table-layout='fixed' width='100%'>
               <fo:table-column column-width='11.5cm' />
               <fo:table-column column-width='6.5cm' />
               <fo:table-body>
                <fo:table-row>
                 <!-- LEFT SECTION: BLOOD WORK CARDIOLOGY ... -->
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:table table-layout='fixed' width='100%'>
                   <fo:table-column column-width='11cm' />
                   <fo:table-body>
                    <fo:table-row>
                     <fo:table-cell border-style='solid' border-width='{$border_width}'>
                      <fo:table table-layout='fixed' width='100%' border-style='solid' border-width='{$border_width}'>
                       <fo:table-column column-width='9.0cm' />
                       <fo:table-column column-width='2.0cm' />
                       <fo:table-body>
                        <fo:table-row>
                         <fo:table-cell padding-top='1.0pt' font-size='16pt' border-bottom-style='dotted'>
                          <fo:block font-family='FormataProMedium'>Blood Work Cardiology Result</fo:block>
                         </fo:table-cell>
                         <fo:table-cell padding-top='1.0pt' font-size='7pt' border-bottom-style='dotted'>
                          <fo:block>
                           <xsl:value-of select='centre' />
                          </fo:block>
                         </fo:table-cell>
                        </fo:table-row>
                       </fo:table-body>
                      </fo:table>
                     </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                     <fo:table-cell border-style='solid' border-width='{$border_width}'>
                      <fo:table table-layout='fixed' width='100%' border-style='solid' border-width='{$border_width}'>
                       <fo:table-column column-width='11cm' />
                       <fo:table-body>
                        <fo:table-row>
                         <fo:table-cell padding-top='6pt' padding-left='10.0pt' font-size='12pt' border-style='solid' border-width='{$border_width}'>
                          <fo:block font-family='FormataProMedium'>Patient</fo:block>
                         </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                         <fo:table-cell padding-top='6pt' padding-left='10.0pt' font-size='8pt' border-style='solid' border-width='{$border_width}'>
                          <fo:block>
                           NAME:
                           <fo:inline font-family='FormataProMedium' font-size="12pt">
                            <xsl:value-of select='patient/name' />
                           </fo:inline>
                          </fo:block>
                         </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                         <fo:table-cell padding-top='6pt' padding-left='10.0pt' font-size='8pt' border-style='solid' border-width='{$border_width}'>
                          <fo:block>
                           GENDER:
                           <fo:inline font-family='FormataProMedium' padding-after='10pt' font-size='10pt'>
                            <xsl:value-of select='patient/gender' />
                           </fo:inline>
                           &#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0; AGE:
                           <fo:inline font-family='FormataProMedium' padding-right='8.0pt' font-size='10pt'>
                            <xsl:value-of select='patient/age' />
                           </fo:inline>
                           &#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0;&#x00A0; DOB:
                           <fo:inline font-family='FormataProMedium' padding-right='8.0pt' font-size='10pt'>
                            <xsl:value-of select='patient/dob' />
                           </fo:inline>
                          </fo:block>
                         </fo:table-cell>
                        </fo:table-row>
                       </fo:table-body>
                      </fo:table>
                     </fo:table-cell>
                    </fo:table-row>
                   </fo:table-body>
                  </fo:table>
                 </fo:table-cell>
                 <!-- RIGHT SECTION: ORDERED BY -->
                 <fo:table-cell padding-top='1.0pt' border-style='solid' border-width='{$border_width}'>
                  <fo:table table-layout='fixed' width='100%' border-style='solid' border-width='{$border_width}'>
                   <fo:table-column column-width='2.5cm' />
                   <fo:table-column column-width='4.0cm' />
                   <fo:table-body>
                    <fo:table-row>
                     <fo:table-cell font-size='8pt' font-weight='bold' padding-right='2.0pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block text-align='right'>ORDERED BY:</fo:block>
                     </fo:table-cell>
                     <fo:table-cell font-size='10pt' font-weight='bold' padding-top='1.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                       <xsl:value-of select='doctor/name' />
                      </fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                     <fo:table-cell font-size='10pt' padding-left='2.0pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block></fo:block>
                     </fo:table-cell>
                     <fo:table-cell font-size='8pt' font-weight='bold' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                       <xsl:value-of select='doctor/company' />
                      </fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                     <fo:table-cell font-size='10pt' padding-left='2.0pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block></fo:block>
                     </fo:table-cell>
                     <fo:table-cell font-size='8pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                       <xsl:value-of select='doctor/email' />
                      </fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                     <fo:table-cell font-size='10pt' padding-left='2.0pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block></fo:block>
                     </fo:table-cell>
                     <fo:table-cell font-size='8pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                       <xsl:value-of select='doctor/phone' />
                      </fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                    <!-- separator -->
                    <fo:table-row>
                     <fo:table-cell padding-top='5.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block />
                     </fo:table-cell>
                     <fo:table-cell padding-top='5.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block />
                     </fo:table-cell>
                    </fo:table-row>
                    <!-- COLLECTED -->
                    <fo:table-row>
                     <fo:table-cell font-size='8pt' font-weight='bold' padding-right='2.0pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block text-align='right'>COLLECTED:</fo:block>
                     </fo:table-cell>
                     <fo:table-cell font-size='8pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                       <xsl:value-of select='collected' />
                      </fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                     <fo:table-cell font-size='8pt' font-weight='bold' padding-right='2.0pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block text-align='right'>RECEIVED:</fo:block>
                     </fo:table-cell>
                     <fo:table-cell font-size='8pt' padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                       <xsl:value-of select='received' />
                      </fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                   </fo:table-body>
                  </fo:table>
                 </fo:table-cell>
                </fo:table-row>
               </fo:table-body>
              </fo:table>
             </fo:table-cell>
            </fo:table-row>
            <!-- DOTTED SEPARATOR -->
            <fo:table-row>
             <xsl:call-template name='dotted-separator' />
            </fo:table-row>
            <!-- ROW 1 -->
            <!-- ABOUT THIS TEST -->
            <fo:table-row>
             <fo:table-cell padding-left='0.5cm' padding-top='5.0pt' padding-bottom='5.0pt' border-style='solid' border-width='{$border_width}'>
              <fo:table table-layout='fixed' width='100%'>
               <fo:table-column column-width='1.0cm' />
               <fo:table-column column-width='17.0cm' />
               <fo:table-body>
                <fo:table-row>
                 <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block>
                   <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                    <svg:svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width='20' height='20'>
                     <svg:circle cx="5" cy="5" r="5" fill="#eb8269" />
                     <svg:text font-size="8" font-weight="bold" fill="#ffffff" font-family="Nimbus Sans L,sans-serif" x="2.5" y="7.5">1</svg:text>
                    </svg:svg>
                   </fo:instream-foreign-object>
                  </fo:block>
                 </fo:table-cell>
                 <fo:table-cell padding-top='10.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block font-family='FormataProMedium' font-size='12pt'>About this test</fo:block>
                 </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:block />
                 </fo:table-cell>
                 <fo:table-cell padding-top='2.0pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block>
                   <xsl:value-of select='about' />
                  </fo:block>
                 </fo:table-cell>
                </fo:table-row>
               </fo:table-body>
              </fo:table>
             </fo:table-cell>
            </fo:table-row>
            <!-- DOTTED SEPARATOR -->
            <fo:table-row>
             <xsl:call-template name='dotted-separator' />
            </fo:table-row>
            <!-- ROW 2 -->
            <!-- GRAPHIC -->
            <fo:table-row>
             <fo:table-cell padding-left='0.5cm' padding-top='5.0pt' padding-bottom='5.0pt' border-style='solid' border-width='{$border_width}'>
              <fo:table table-layout='fixed' width='100%'>
               <fo:table-column column-width='1.0cm' />
               <fo:table-column column-width='17.0cm' />
               <fo:table-body>
                <fo:table-row>
                 <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block>
                   <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                    <svg:svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width='20' height='20'>
                     <svg:circle cx="5" cy="5" r="5" fill="#eb8269" />
                     <svg:text font-size="8" font-weight="bold" fill="#ffffff" font-family="Nimbus Sans L,sans-serif" x="2.5" y="7.5">2</svg:text>
                    </svg:svg>
                   </fo:instream-foreign-object>
                  </fo:block>
                 </fo:table-cell>
                 <fo:table-cell padding-top='10.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block font-family='FormataProMedium' font-size='12pt'>Your results</fo:block>
                 </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:block />
                 </fo:table-cell>
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:block>
                   <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                    <xsl:value-of disable-output-escaping="yes" select="chart:getCardioSVG(17.0, 11.0, 3.3, 265, 233, 32)" />
                   </fo:instream-foreign-object>
                  </fo:block>
                 </fo:table-cell>
                </fo:table-row>
               </fo:table-body>
              </fo:table>
             </fo:table-cell>
            </fo:table-row>
            <!-- DOTTED SEPARATOR -->
            <fo:table-row>
             <xsl:call-template name='dotted-separator' />
            </fo:table-row>
            <!-- ROW 3 -->
            <!-- YOUR RISK -->
            <fo:table-row>
             <fo:table-cell padding-left='0.5cm' padding-top='5.0pt' padding-bottom='5.0pt' border-style='solid' border-width='{$border_width}'>
              <fo:table table-layout='fixed' width='100%'>
               <fo:table-column column-width='1.0cm' />
               <fo:table-column column-width='17.0cm' />
               <fo:table-body>
                <fo:table-row>
                 <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block>
                   <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                    <svg:svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width='20' height='20'>
                     <svg:circle cx="5" cy="5" r="5" fill="#eb8269" />
                     <svg:text font-size="8" font-weight="bold" fill="#ffffff" font-family="Nimbus Sans L,sans-serif" x="2.5" y="7.5">3</svg:text>
                    </svg:svg>
                   </fo:instream-foreign-object>
                  </fo:block>
                 </fo:table-cell>
                 <fo:table-cell padding-top='10.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block font-family='FormataProMedium' font-size='12pt'>
                   Your risk
                   <fo:inline font-family='FormataProLight'>You show an elevated risk of cardiovascular disease</fo:inline>
                  </fo:block>
                 </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:block />
                 </fo:table-cell>
                 <fo:table-cell padding-top='6pt' border-style='solid' border-width='{$border_width}'>
                  <fo:table table-layout='fixed' width='100%' border-style='solid' border-width='{$border_width}' font-size='8pt'>
                   <fo:table-column column-width='4.5cm' />
                   <fo:table-column column-width='4.0cm' />
                   <fo:table-column column-width='8.0cm' />
                   <fo:table-body>
                    <fo:table-row>
                     <fo:table-cell padding-right='2.0pt' padding-top='1.0pt' line-height='11pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>If you are smoker with blood presure of 130mm/Hg but a family history of heart attack before age 60 (in one or both parents) your risk over the next 10 years is: 
                      </fo:block>
                     </fo:table-cell>
                     <fo:table-cell text-align='center' font-size='55pt' padding-top='1.0pt' padding-left='4.0pt' color='#19824a' border-style='solid' border-width='{$border_width}'>
                      <fo:block font-weight='bold'>
                       <xsl:value-of select='result/risk' />
                       <fo:inline font-size='30pt'>%</fo:inline>
                      </fo:block>
                     </fo:table-cell>
                     <fo:table-cell line-height='11pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block font-weight='bold'>Your risk would be lowered to:</fo:block>
                      <fo:block font-weight='bold'>
                       12%
                       <fo:inline font-weight='normal'>if your blood pressure were 120mm/HG</fo:inline>
                      </fo:block>
                      <fo:block font-weight='bold'>
                       10%
                       <fo:inline font-weight='normal'>if you quit smoking</fo:inline>
                      </fo:block>
                      <fo:block font-weight='bold'>
                       &#160;&#160;6%
                       <fo:inline font-weight='normal'>if you reduced your cholesterol to 160 mg/DL</fo:inline>
                      </fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                   </fo:table-body>
                  </fo:table>
                 </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:block />
                 </fo:table-cell>
                 <fo:table-cell padding-top='8pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block font-size='8pt'>
                   Use your CRP results and cholesterol level to calculate your 10-year risk of a cardiovascular event a
                   <fo:inline font-weight='bold'>
                    <fo:basic-link external-destination="www.reynoldsriskcore.org" text-decoration="underline" color="blue">www.reynoldsriskcore.org</fo:basic-link>
                   </fo:inline>
                  </fo:block>
                 </fo:table-cell>
                </fo:table-row>
               </fo:table-body>
              </fo:table>
             </fo:table-cell>
            </fo:table-row>
            <!-- ROW 4 -->
            <!-- WHAT NOW? -->
            <fo:table-row>
             <fo:table-cell padding-left='0.5cm' padding-top='5.0pt' padding-bottom='5.0pt' border-style='solid' border-width='{$border_width}'>
              <fo:table table-layout='fixed' width='100%'>
               <fo:table-column column-width='1.0cm' />
               <fo:table-column column-width='17.0cm' />
               <fo:table-body>
                <fo:table-row>
                 <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block>
                   <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                    <svg:svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10" width='20' height='20'>
                     <svg:circle cx="5" cy="5" r="5" fill="#eb8269" />
                     <svg:text font-size="8" font-weight="bold" fill="#ffffff" font-family="Nimbus Sans L,sans-serif" x="2.5" y="7.5">4</svg:text>
                    </svg:svg>
                   </fo:instream-foreign-object>
                  </fo:block>
                 </fo:table-cell>
                 <fo:table-cell padding-top='10.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                  <fo:block font-family='FormataProMedium' font-size='12pt'>What now ?</fo:block>
                 </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:block />
                 </fo:table-cell>
                 <fo:table-cell border-style='solid' border-width='{$border_width}'>
                  <fo:table table-layout='fixed' width='100%' border-style='solid' border-width='{$border_width}' font-size='8pt'>
                   <fo:table-column column-width='1.5cm' />
                   <fo:table-column column-width='2.6cm' />
                   <fo:table-column column-width='1.5cm' />
                   <fo:table-column column-width='2.6cm' />
                   <fo:table-column column-width='1.5cm' />
                   <fo:table-column column-width='2.6cm' />
                   <fo:table-column column-width='1.5cm' />
                   <fo:table-column column-width='2.6cm' />
                   <fo:table-body>
                    <fo:table-row>
                     <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                      <!-- Image of Men Running -->
                       <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                        <svg:svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="40.000000pt" height="50.000000pt" viewBox="0 0 75.000000 91.000000" preserveAspectRatio="xMidYMid meet">
                         <svg:g transform="translate(0.000000,91.000000) scale(0.10000,-0.10000)" fill='#19824a' stroke="none">
                          <svg:path
                           d="M553 753 c-8 -2 -13 -22 -13 -44 0 -29 -5 -41 -17 -46 -25 -9 -64 -40 -81 -64 -11 -16 -11 -23 -2 -34 20 -24 -4 -134 -39 -184 -12 -16 -20 -35
										  	-19 -42 3 -14 -9 -16 -101 -22 -56 -4 -63 -10 -75 -62 l-8 -30 24 22 c35 33 74 44 138 39 51 -4 61 -2 80 19 20 21 45 25 93 16 4 0 7 -16 7 -35 0 -18 7
										  	-53 15 -76 8 -23 15 -53 15 -66 0 -24 7 -28 45 -25 63 5 94 31 37 31 -46 0 -67 32 -68 104 -1 57 -25 147 -46 171 -26 30 11 89 53 83 12 -2 24 3 27 11 3
										  	10 -1 12 -16 9 -32 -9 -46 5 -32 32 7 13 9 36 5 56 -4 26 -2 36 10 40 9 4 22 19 30 34 23 45 -14 83 -62 63z" />
                         </svg:g>
                        </svg:svg>
                       </fo:instream-foreign-object>
                      </fo:block>
                     </fo:table-cell>
                     <fo:table-cell padding-top='2.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block font-weight='bold'>Diet and exercise</fo:block>
                      <fo:block>can improve your cholesterol levels</fo:block>
                     </fo:table-cell>
                     <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                      <!-- Image of Men Drinking -->
                       <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                        <svg:svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="28.000000pt" height="50.000000pt" viewBox="0 0 58.000000 79.000000" preserveAspectRatio="xMidYMid meet">
                         <svg:g transform="translate(0.000000,79.000000) scale(0.100000,-0.100000)" fill="#19824a" stroke="none">
                          <svg:path
                           d="M338 711 c-16 -15 -28 -31 -28 -34 0 -4 -9 -16 -20 -27 -24 -24 -59
									  	-25 -126 -5 -27 8 -62 15 -77 15 -31 0 -45 -23 -27 -45 12 -15 13 -31 9 -190
									  	-3 -144 6 -154 120 -136 35 6 68 8 72 5 5 -3 9 -53 9 -110 l0 -106 125 4 c68
									  	2 128 8 133 13 11 13 -3 229 -17 277 -6 20 -27 59 -46 87 -19 28 -35 53 -35
									  	56 0 4 13 17 29 31 59 49 48 114 -27 164 -25 16 -50 30 -56 30 -6 0 -23 -13
									  	-38 -29z m-80 -118 c-1 -10 10 -31 25 -47 20 -22 27 -38 25 -59 -3 -27 -11
									  	-34 -76 -63 l-73 -33 -14 32 c-7 18 -17 61 -21 95 -9 81 0 92 78 92 51 0 59
									  	-2 56 -17z" />
                         </svg:g>
                        </svg:svg>
                       </fo:instream-foreign-object>
                      </fo:block>
                     </fo:table-cell>
                     <fo:table-cell padding-top='2.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block font-weight='bold'>Avoid drinking</fo:block>
                      <fo:block>alcohol, except in moderation: one to two drinks per day</fo:block>
                     </fo:table-cell>
                     <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                       <!-- Image of Men doctor -->
                       <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                        <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="39.000000pt" height="50.000000pt" viewBox="0 0 69.000000 81.000000" preserveAspectRatio="xMidYMid meet">
                         <g transform="translate(0.000000,81.000000) scale(0.100000,-0.100000)" fill="#19824a" stroke="none">
                          <path
                           d="M433 733 c-18 -7 -16 -74 2 -101 19 -26 8 -54 -27 -73 -43 -23 -121
											  	-38 -168 -32 -26 3 -55 1 -65 -4 -13 -8 -1 -11 50 -15 67 -6 104 -25 91 -47
											  	-4 -6 -39 -11 -82 -11 -82 0 -114 -11 -114 -39 0 -10 -7 -24 -16 -32 -19 -16
											  	-15 -44 23 -146 15 -40 33 -73 40 -73 16 0 17 -12 1 -28 -9 -9 -7 -12 10 -12
											  	37 0 79 27 86 55 7 28 48 49 77 40 24 -8 23 -14 -1 -45 -21 -27 -27 -70 -9
											  	-70 6 0 8 7 5 16 -9 24 25 76 46 70 10 -2 18 0 18 5 0 25 48 5 87 -36 24 -25
											  	43 -41 43 -36 0 5 -19 29 -42 52 -24 24 -42 47 -40 52 1 5 -8 7 -20 5 -17 -2
											  	-24 3 -26 19 -3 21 -4 21 -23 5 -10 -9 -43 -22 -72 -29 -43 -9 -55 -9 -68 3
											  	-17 15 -46 91 -37 99 2 3 26 8 52 11 43 6 48 5 53 -15 4 -17 13 -21 47 -22 73
											  	-2 100 3 112 18 8 12 13 13 18 5 14 -24 24 -11 31 41 4 28 21 81 37 116 l29
											  	63 -23 27 c-13 14 -29 30 -36 35 -10 6 -12 20 -7 52 8 56 -7 84 -43 83 -15 0
											  	-33 -3 -39 -6z" />
                         </g>
                        </svg>
                       </fo:instream-foreign-object>
                      </fo:block>
                     </fo:table-cell>
                     <fo:table-cell padding-top='2.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block font-weight='bold'>Ask your doctor</fo:block>
                      <fo:block>about statins or other medications that can lower cholesterol</fo:block>
                     </fo:table-cell>
                     <fo:table-cell padding-top='5pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block>
                      <!-- Image of (TODO) -->
                       <fo:instream-foreign-object xmlns:svg="http://www.w3.org/2000/svg">
                        <svg:svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="40.000000pt" height="50.000000pt" viewBox="0 0 40.000000 39.000000" preserveAspectRatio="xMidYMid meet">
                         <svg:g transform="translate(0.000000,39.000000) scale(0.100000,-0.100000)" fill="#19824a" stroke="none">
                         </svg:g>
                        </svg:svg>
                       </fo:instream-foreign-object>
                      </fo:block>
                     </fo:table-cell>
                     <fo:table-cell padding-top='2.0pt' padding-bottom='2.0pt' border-style='solid' border-width='{$border_width}'>
                      <fo:block font-weight='bold'>Consider retesting</fo:block>
                      <fo:block>in one to two weeks in case your CRP level was caused by infection</fo:block>
                     </fo:table-cell>
                    </fo:table-row>
                   </fo:table-body>
                  </fo:table>
                 </fo:table-cell>
                </fo:table-row>
               </fo:table-body>
              </fo:table>
             </fo:table-cell>
            </fo:table-row>
            <!-- DOTTED SEPARATOR -->
            <fo:table-row>
             <xsl:call-template name='dotted-separator' />
            </fo:table-row>
           </fo:table-body>
          </fo:table>
          <!-- BACKGROUND COLOR CELL END -->
         </fo:table-cell>
        </fo:table-row>
       </fo:table-body>
      </fo:table>
      <!-- If is not the last postion then insert a page break between 2 request patient. -->
      <xsl:if test="position() != last()">
       <fo:block break-after='page' />
      </xsl:if>
     </xsl:for-each>
    </fo:flow>
   </fo:page-sequence>
   <!-- END PAGE SEQUENCE -->
  </fo:root>
 </xsl:template>
</xsl:stylesheet>