#!/bin/bash
#
# ----------------------------- jkscertgen Modern Version -----------------------------
#
# Updated to work with modern versions of Java (keytool) and OpenSSL.
# Original script by Kamal Wicramanayake
# Updated by [Your Name]
#
# Note: This script generates four JKS keystores (CA, service, client1, client2),
# signs certificates using a self-signed CA, and organizes the generated files
# into specific directories.
#
# Dependencies: Java (keytool), OpenSSL
#

# --- Edit the following to match with your preferences ---
# --- Default values also work fine ---

# Uncomment the following line if you plan to run this command multiple
# times so that all the files generated will be deleted before fresh files
# are generated.
rm -f *.cer *.jks *.csr *.key *.p12 *.srl openssl.cnf *.pem

# Create directories if they don't exist
mkdir -p ../certificate-requests-only
mkdir -p ../certificates-only
mkdir -p ../jks-files
mkdir -p ../jack-files

# Specify the key algorithm
KEYALG=RSA

# Add the generated client certificates to service keystore? [YES/NO]
ADD_CLIENT_TO_SERVICE=YES

# To slow down the process for you to see the steps, mention seconds
# to sleep after major steps. Specify 0 or a positive integer.
SLEEP=1

# CA Details
CA=swviewca
CA_DNAME="CN=Software View Certificate Authority, OU=Training, O=Software View, L=Colombo, ST=Western, C=LK"
CA_KEYSTOREPASS=swviewcastoresecret
CA_VALIDITY=6000

# Test Service Details
SERVICE=myservice
SERVICE_DNAME="CN=My Test Service, OU=Training, O=Software View, L=Colombo, ST=Western, C=LK"
SERVICE_KEYSTOREPASS=myservicestoresecret
SERVICE_VALIDITY=5500
CA_SERVICE_VALIDITY=5000

# Test Client 1 Details
CLIENT1=johnnie
CLIENT1_DNAME="CN=Johnnie Walker, OU=Training, O=Software View, L=Colombo, ST=Western, C=LK"
CLIENT1_KEYSTOREPASS=johnniestoresecret
CLIENT1_VALIDITY=5500
CA_CLIENT1_VALIDITY=5000

# Test Client 2 Details
CLIENT2=jack
CLIENT2_DNAME="CN=Jack Daniel, OU=Training, O=Software View, L=Colombo, ST=Western, C=LK"
CLIENT2_KEYSTOREPASS=secret
CLIENT2_VALIDITY=5500
CA_CLIENT2_VALIDITY=5000

# All key passwords are set to the same value as the keystore password
CA_KEYPASS=$CA_KEYSTOREPASS
SERVICE_KEYPASS=$SERVICE_KEYSTOREPASS
CLIENT1_KEYPASS=$CLIENT1_KEYSTOREPASS
CLIENT2_KEYPASS=$CLIENT2_KEYSTOREPASS

# --- You need not edit what follows ---

# Check if keytool and openssl commands exist
if ! command -v keytool >/dev/null 2>&1; then
    echo "keytool command cannot be found. Haven't you installed Java (JDK)?"
    exit 1
fi

if ! command -v openssl >/dev/null 2>&1; then
    echo "openssl command cannot be found. Haven't you installed OpenSSL?"
    exit 2
fi

# Write the openssl.cnf file
echo "Writing an openssl.cnf file"
cat > openssl.cnf << EOF

[ v3_ca ]
basicConstraints = critical,CA:true
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer:always

[ v3_usr ]
basicConstraints = critical,CA:false
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
nsComment = "OpenSSL Generated Certificate"

EOF

# Generate CA keys
echo "Generating CA keys"
keytool -genkeypair -keyalg $KEYALG -keysize 2048 \
    -alias $CA -dname "$CA_DNAME" \
    -keystore $CA.jks -storepass $CA_KEYSTOREPASS \
    -validity $CA_VALIDITY

# Generate CA certificate signing request
echo "Generate CA certificate signing request"
keytool -certreq -alias $CA \
    -file $CA.csr \
    -keystore $CA.jks -storepass $CA_KEYSTOREPASS

# Export CA private key and certificate to PKCS12 store
echo "Exporting CA private key and certificate to PKCS12 store"
keytool -importkeystore -srckeystore $CA.jks \
    -destkeystore $CA.p12 \
    -srcstoretype JKS -deststoretype PKCS12 \
    -srcalias $CA -deststorepass $CA_KEYSTOREPASS \
    -srcstorepass $CA_KEYSTOREPASS -noprompt

# Convert PKCS12 to PEM
echo "Convert PKCS12 to PEM"
openssl pkcs12 -in $CA.p12 -out $CA.pem \
    -passin pass:$CA_KEYSTOREPASS -passout pass:$CA_KEYSTOREPASS

# Self-sign the CA certificate with extensions
echo "Adding general CA extensions to CA certificate and self-signing"
openssl x509 -req -days $CA_VALIDITY -in $CA.csr \
    -signkey $CA.pem -out $CA.cer \
    -passin pass:$CA_KEYSTOREPASS \
    -extfile openssl.cnf -extensions v3_ca

# Import the self-signed CA certificate back into the CA keystore
echo "Importing CA self-signed certificate with extensions to CA keystore"
keytool -importcert -alias $CA \
    -file $CA.cer \
    -keystore $CA.jks -storepass $CA_KEYSTOREPASS -noprompt

# Clean up intermediate files
rm -f $CA.p12 $CA.pem

echo ""
echo "CA Keys and Self-Signed Certificate Generation Complete"
echo "About to generate [$SERVICE] keys/certificates..."
sleep $SLEEP

# Generate service keys
echo "Generating $SERVICE keys"
keytool -genkeypair -keyalg $KEYALG -keysize 2048 \
    -alias $SERVICE -dname "$SERVICE_DNAME" \
    -keystore $SERVICE.jks -storepass $SERVICE_KEYSTOREPASS \
    -validity $SERVICE_VALIDITY

# Generate service CSR
echo "Generating $SERVICE CSR"
keytool -certreq -alias $SERVICE \
    -file $SERVICE.csr \
    -keystore $SERVICE.jks -storepass $SERVICE_KEYSTOREPASS

# CA signs the service certificate
echo "CA signs $SERVICE certificate"
openssl x509 -req -days $CA_SERVICE_VALIDITY -in $SERVICE.csr \
    -CA $CA.cer -CAkey $CA.pem -CAcreateserial \
    -out $SERVICE.cer -passin pass:$CA_KEYSTOREPASS \
    -extfile openssl.cnf -extensions v3_usr

# Import CA certificate into service keystore
echo "Import CA trusted certificate into $SERVICE keystore"
keytool -importcert -alias $CA -file $CA.cer \
    -keystore $SERVICE.jks -storepass $SERVICE_KEYSTOREPASS -noprompt

# Import signed service certificate into service keystore
echo "Import CA signed $SERVICE certificate into $SERVICE keystore"
keytool -importcert -alias $SERVICE -file $SERVICE.cer \
    -keystore $SERVICE.jks -storepass $SERVICE_KEYSTOREPASS -noprompt

echo ""
echo "[$SERVICE] Keys and Certificate Generation Complete"
echo "About to generate [$CLIENT1] keys/certificates..."
sleep $SLEEP

# Generate client1 keys
echo "Generating $CLIENT1 keys"
keytool -genkeypair -keyalg $KEYALG -keysize 2048 \
    -alias $CLIENT1 -dname "$CLIENT1_DNAME" \
    -keystore $CLIENT1.jks -storepass $CLIENT1_KEYSTOREPASS \
    -validity $CLIENT1_VALIDITY

# Generate client1 CSR
echo "Generating $CLIENT1 CSR"
keytool -certreq -alias $CLIENT1 \
    -file $CLIENT1.csr \
    -keystore $CLIENT1.jks -storepass $CLIENT1_KEYSTOREPASS

# CA signs the client1 certificate
echo "CA signs $CLIENT1 certificate"
openssl x509 -req -days $CA_CLIENT1_VALIDITY -in $CLIENT1.csr \
    -CA $CA.cer -CAkey $CA.pem -CAserial $CA.srl \
    -out $CLIENT1.cer -passin pass:$CA_KEYSTOREPASS \
    -extfile openssl.cnf -extensions v3_usr

# Import CA certificate into client1 keystore
echo "Import CA trusted certificate into $CLIENT1 keystore"
keytool -importcert -alias $CA -file $CA.cer \
    -keystore $CLIENT1.jks -storepass $CLIENT1_KEYSTOREPASS -noprompt

# Import signed client1 certificate into client1 keystore
echo "Import CA signed $CLIENT1 certificate into $CLIENT1 keystore"
keytool -importcert -alias $CLIENT1 -file $CLIENT1.cer \
    -keystore $CLIENT1.jks -storepass $CLIENT1_KEYSTOREPASS -noprompt

# Import service certificate into client1 keystore
echo "Import $SERVICE certificate into $CLIENT1 keystore"
keytool -importcert -alias $SERVICE -file $SERVICE.cer \
    -keystore $CLIENT1.jks -storepass $CLIENT1_KEYSTOREPASS -noprompt

echo ""
echo "[$CLIENT1] Keys and Certificate Generation Complete"
echo "About to generate [$CLIENT2] keys/certificates..."
sleep $SLEEP

# Generate client2 keys
echo "Generating $CLIENT2 keys"
keytool -genkeypair -keyalg $KEYALG -keysize 2048 \
    -alias $CLIENT2 -dname "$CLIENT2_DNAME" \
    -keystore $CLIENT2.jks -storepass $CLIENT2_KEYSTOREPASS \
    -validity $CLIENT2_VALIDITY

# Generate client2 CSR
echo "Generating $CLIENT2 CSR"
keytool -certreq -alias $CLIENT2 \
    -file $CLIENT2.csr \
    -keystore $CLIENT2.jks -storepass $CLIENT2_KEYSTOREPASS

# CA signs the client2 certificate
echo "CA signs $CLIENT2 certificate"
openssl x509 -req -days $CA_CLIENT2_VALIDITY -in $CLIENT2.csr \
    -CA $CA.cer -CAkey $CA.pem -CAserial $CA.srl \
    -out $CLIENT2.cer -passin pass:$CA_KEYSTOREPASS \
    -extfile openssl.cnf -extensions v3_usr

# Import CA certificate into client2 keystore
echo "Import CA trusted certificate into $CLIENT2 keystore"
keytool -importcert -alias $CA -file $CA.cer \
    -keystore $CLIENT2.jks -storepass $CLIENT2_KEYSTOREPASS -noprompt

# Import signed client2 certificate into client2 keystore
echo "Import CA signed $CLIENT2 certificate into $CLIENT2 keystore"
keytool -importcert -alias $CLIENT2 -file $CLIENT2.cer \
    -keystore $CLIENT2.jks -storepass $CLIENT2_KEYSTOREPASS -noprompt

# Import service certificate into client2 keystore
echo "Import $SERVICE certificate into $CLIENT2 keystore"
keytool -importcert -alias $SERVICE -file $SERVICE.cer \
    -keystore $CLIENT2.jks -storepass $CLIENT2_KEYSTOREPASS -noprompt

echo ""
echo "[$CLIENT2] Keys and Certificate Generation Complete"

if [ "$ADD_CLIENT_TO_SERVICE" == "YES" ]; then
    echo "About to add client certificates to service keystore"
    sleep $SLEEP

    # Import client1 certificate into service keystore
    echo "Import $CLIENT1 certificate into $SERVICE keystore"
    keytool -importcert -alias $CLIENT1 -file $CLIENT1.cer \
        -keystore $SERVICE.jks -storepass $SERVICE_KEYSTOREPASS -noprompt

    # Import client2 certificate into service keystore
    echo "Import $CLIENT2 certificate into $SERVICE keystore"
    keytool -importcert -alias $CLIENT2 -file $CLIENT2.cer \
        -keystore $SERVICE.jks -storepass $SERVICE_KEYSTOREPASS -noprompt
fi

echo -e "\n\nYou will retain all .jks files. If you plan to sign more certificates with $CA, you may wish to retain $CA.pem and $CA.srl. $CA.pem contains the $CA's private key and self-signed certificate. $CA.srl contains a serial key that increments automatically and goes into each certificate signed. Other files can be deleted and regenerated from .jks files if required."

echo -e "\nYou may verify the results with the following commands with correct values for the file and password:"
echo "    \$ keytool -list -keystore file.jks -storepass password"
echo "    \$ keytool -v -list -keystore file.jks -storepass password | less"
echo "    \$ keytool -printcert -file file.cer"
echo "    \$ cat file.pem"

echo "MOVING FILES"
cp *.csr ../certificate-requests-only/
cp *.cer ../certificates-only/
cp *.jks ../jks-files/
cp jack.* ../jack-files
