<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:xbrli="http://www.xbrl.org/2003/instance"
	xmlns:ix="http://www.xbrl.org/2008/inlineXBRL"
	xmlns:ixt="http://www.xbrl.org/inlineXBRL/transformation/2010-04-20"
	xmlns:link="http://www.xbrl.org/2003/linkbase"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:iso4217="http://www.xbrl.org/2003/iso4217"
	xmlns:xbrldi="http://xbrl.org/2006/xbrldi"
	xmlns:xbrldt="http://xbrl.org/2005/xbrldt"
	xmlns:xsltc="http://xml.apache.org/xalan/xsltc"
	xmlns:axional="deister.axional.script.lib.ax.xbrl.impl.XSLToolkit"
	exclude-result-prefixes="#all">
	<!-- WARNING: do not change path 
	deister.axional.script.lib.ax.xbrl.impl.XSLToolkit
	-->
<!-- 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	
 -->
	<xsl:output method="xml" 
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" 
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" 
		indent="yes"/>
   
   <!--  VARIABLES injected during transformation -->
	<xsl:param name="css" />
	<xsl:param name="fonts" />
	<xsl:param name="images" />
	 
   <!-- 
	<xsl:variable name="path">src/test/java/deister/axional/server/script/lib/ax/xbrl</xsl:variable>
    -->
   	<!-- handle the root XML element -->
	<xsl:template match="/">
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>GLEIF Annual Accounts</title>
				
				<xsl:element name="style">
					<xsl:attribute name="type">text/css</xsl:attribute>
					@font-face {
					  font-family: Vegur;
					  src: url(data:font/opentype;charset=utf-8;base64,<xsl:value-of select="axional:get($fonts, 'regular')"/>);
					}
					@font-face {
					  font-family: VegurBold;
					  src: url(data:font/opentype;charset=utf-8;base64,<xsl:value-of select="axional:get($fonts, 'bold')"/>);
					}
					@font-face {
					  font-family: VegurLight;
					  src: url(data:font/opentype;charset=utf-8;base64,<xsl:value-of select="axional:get($fonts, 'light')"/>);
					}
				</xsl:element>
				
				<xsl:element name="link">
					<xsl:attribute name="type">text/css</xsl:attribute>
					<xsl:attribute name="rel">stylesheet</xsl:attribute>
					<xsl:attribute name="href">data:text/css;base64,<xsl:value-of select="$css"/></xsl:attribute>
				</xsl:element>
				
				<meta name="viewport" content="width=device-width, initial-scale=1" />
					
				<style type="text/css">
.linked-highlight-text {
	outline: dashed 2px #026dce;
	outline-offset: 1px
}

.linked-highlight-cell {
	outline: dashed 2px #026dce;
	outline-offset: 1px;
	outline-offset: -1px
}

.iv-ixbrl-fact {
	background-color: yellow;
	cursor: pointer
}

.ixbrl-highlight {
	background-color: #6cff6c !important
}

.ixbrl-highlight.ixbrl-highlight-1 {
	background-color: #ffff3c !important
}

.ixbrl-highlight.ixbrl-highlight-2 {
	background-color: #eaa8ff !important
}

div.ixbrl-selected,
span.ixbrl-selected {
	outline: solid 2px #026dce;
	outline-offset: 1px
}

div.ixbrl-element:hover:not(.ixbrl-selected),
span.ixbrl-element:hover:not(.ixbrl-selected) {
	outline: dashed 2px #026dce;
	outline-offset: 1px
}

.ixbrl-element {
	cursor: pointer
}

.ixbrl-related {
	outline: dashed 2px #0eb30e;
	outline-offset: 1px
}

td.ixbrl-selected,
th.ixbrl-selected {
	outline: solid 2px #026dce !important;
	outline-offset: -1px !important
}

td.ixbrl-element:hover:not(.ixbrl-selected),
th.ixbrl-element:hover:not(.ixbrl-selected) {
	outline: dashed 2px #026dce !important;
	outline-offset: -1px !important
}

td.ixbrl-related,
th.ixbrl-related {
	outline-offset: -1px
}

.ixbrl-related.ixbrl-linked-highlight,
.ixbrl-linked-highlight {
	outline: dashed 2px #026dce;
	outline-offset: 1px
}

td.ixbrl-related.ixbrl-linked-highlight,
td.ixbrl-linked-highlight {
	outline: dashed 2px #026dce;
	outline-offset: 1px;
	outline-offset: -1px
}

div.ixbrl-table-handle {
	position: absolute;
	left: 0;
	cursor: pointer;
	width: 0;
	height: 0;
	border: .4rem solid transparent;
	border-bottom-color: #66ca00;
	top: -0.8rem
}

div.ixbrl-table-handle:after {
	content: '';
	position: absolute;
	left: -0.4rem;
	top: .4rem;
	width: 0;
	height: 0;
	border: .4rem solid transparent;
	border-top-color: #66ca00
}

div.ixbrl-table-handle>span {
	display: none;
	white-space: nowrap
}

div.ixbrl-table-handle:hover {
	border: none;
	color: #fff;
	transition: width .3s;
	width: 9rem;
	height: initial;
	left: -0.1rem;
	padding: .7rem 0 0 0
}

div.ixbrl-table-handle:hover:after {
	display: none
}

div.ixbrl-table-handle:hover>span {
	border: solid .1rem #fff;
	font-family: sans-serif;
	display: block;
	padding: 7px 15px;
	background-color: #66ca00
}

div.ixbrl-table-handle:hover>span:before {
	font-family: "icomoon";
	content: "\e95e";
	padding-right: 7px;
	position: relative;
	top: 1px
}

				</style>
			</head>
			<body>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>

	<!-- =========================================================== -->
	<!-- root -->
	<!-- =========================================================== -->

	<xsl:template match="root">
		<xsl:apply-templates />
	</xsl:template>

	<!-- =========================================================== -->
	<!-- ix-document / ix-header -->
	<!-- =========================================================== -->

	<xsl:template match="ix-document">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="ix:header">
		<xsl:comment>
			IX:HEADER (AUTOMATIC GENERATION)
		</xsl:comment>
		<div style="display: none">
			<xsl:element name="ix:header">
				<xsl:copy-of select="@*|node()"/>
			</xsl:element>
		</div>
		<xsl:comment>
			IX:HEADER (END)
		</xsl:comment>
    </xsl:template>

	<xsl:template match="ix:nonfraction">
		<span class="ixbrl-element ixbrl-element-nonfraction">
			<xsl:element name="ix:nonfraction">
				<xsl:copy-of select="@*|node()"/>
			</xsl:element>
		</span>
    </xsl:template>

	<!-- =========================================================== -->
	<!-- ONEPAGE -->
	<!-- =========================================================== -->
	<xsl:template match="onepage">

		<div class="onepage">
			<xsl:apply-templates select="page" />
		</div>
	</xsl:template>

	<!-- =========================================================== -->
	<!-- TWOPAGE -->
	<!-- =========================================================== -->
	<xsl:template match="twopage">

		<div class="twopage">

			<xsl:apply-templates select="page" />

		</div>
	</xsl:template>

	<!-- =========================================================== -->
	<!-- PAGE -->
	<!-- =========================================================== -->

	<xsl:template match="page">

		<!-- debug -->
		<!-- 
		<p style='color:#ffffff'>
			debug page
			<xsl:value-of select="@type" />
		</p>
 		-->
 
		<div>
			<!-- <div class="page narrow"> -->
			<xsl:attribute name="class">page <xsl:value-of select="@type" /></xsl:attribute>

			<!-- TODO!! move GLEIF from CSS!! and use /report/@company -->
			<div class="page-header">
				<div class="title">
					<xsl:value-of select="/root/ix-document/@title" />
					<xsl:value-of select="/root/ix-document/@year" />
					|
				</div>
			</div>
			<xsl:apply-templates />
		</div>

	</xsl:template>

	<xsl:template match="page-body">
		<div class="body">
			<xsl:apply-templates />
		</div>
	</xsl:template>


	<xsl:template match="page-main">
		<div class="main">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match="page-title">
		<h3>
			<xsl:apply-templates />
		</h3>
	</xsl:template>

	<xsl:template match="page-subtitle">
		<!-- use space and in-line as we consume spaces using normalize-space() -->	
		<span class="subtitle">&#160;<xsl:apply-templates /></span>
	</xsl:template>

	<xsl:template match="caption">	
		<div class="caption">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match="bottom-caption">	
		<div class="bottom-caption">
			<xsl:apply-templates />
		</div>
	</xsl:template>



	<!-- ===========================================================  
		 QUADRANT
	
	 	 =========================================================== -->

	<xsl:template match="quadrant">
		<xsl:element name="div">
			<xsl:attribute name="class">quadrant <xsl:value-of select="@position"/></xsl:attribute>
			
			<xsl:choose>
			    <xsl:when test="@image">
					<xsl:element name="img">
						<xsl:attribute name="class">background</xsl:attribute>
						<xsl:attribute name="src">data:image/png;base64,<xsl:value-of select="axional:get($images, @image)"/></xsl:attribute>
					</xsl:element>
					<xsl:choose>
					    <xsl:when test="@top-logo">
							<div class="top-logo">
								<xsl:element name="img">
									<xsl:attribute name="class">top-logo</xsl:attribute>
									<xsl:attribute name="src">data:image/png;base64,<xsl:value-of select="axional:get($images, @top-logo)"/></xsl:attribute>
								</xsl:element>
							</div>	
					    </xsl:when>
					</xsl:choose>
				</xsl:when>
		  	</xsl:choose>
	  		<xsl:apply-templates />
		</xsl:element>

	</xsl:template>


	<!-- ===========================================================  
		 XLINK 
			
			A column in a table can have aspect="link"
			This means the content of the column is a reference to a
			section in the document. For example: "3.1" In that cases
			the column will contain a internal href to the document as:
			
			<a href="3_1">3.1</a>
			
			When user writes the XML with the document, he must mark
			the sections with the anchor xlink. For example:
			
			<xlink>3.1<xlink>Fee Revenues
			
	 	 =========================================================== -->

	<xsl:template match="xlink">
		<xsl:element name="a">
			<xsl:attribute name="id">_<xsl:value-of select="translate(current(), '.', '_')"></xsl:value-of></xsl:attribute>
			<xsl:value-of select="current()"/>&#160;
		</xsl:element>
	</xsl:template>

	<!-- =========================================================== -->
	<!-- XTABLE -->
	<!-- =========================================================== -->

	
	<xsl:template match="xtable">
		<xsl:comment>
			XTABLE
		</xsl:comment>

		<!-- <table class="accounts" style="position: relative;"> -->
		<xsl:element name="table">
			
			<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
			<xsl:attribute name="style">position: relative;</xsl:attribute>

			<xsl:apply-templates />
			
			<div class="ixbrl-table-handle">
				<span>Export table</span>
			</div>
		</xsl:element>

	</xsl:template>


	<xsl:template match="xthead">
		<thead>
    		<xsl:for-each select="rows/row">
				<xsl:element name="tr">
					<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
		    		<xsl:for-each select="col">
						<xsl:choose>
						    <xsl:when test="position() > 1">
						    	<th class="spacing" />
						    </xsl:when>
						</xsl:choose>
		    			<!-- Need to store column position to do the == position() or will get all col attributes (why ... ?) -->
    					<xsl:variable name="columnPosition"><xsl:value-of select="position()"/></xsl:variable>
						<xsl:element name="th">
							<xsl:attribute name="class"><xsl:value-of select="../../../../cols/col[position() = $columnPosition]/@class"/></xsl:attribute>
							<xsl:attribute name="style"><xsl:value-of select="../../../../cols/col[position() = $columnPosition]/@style"/></xsl:attribute>
							<xsl:apply-templates />
						</xsl:element>
					</xsl:for-each>
				</xsl:element>
			</xsl:for-each>	
		</thead>
	</xsl:template>

	<xsl:template match="xtbody">
		<tbody>
    		<xsl:for-each select="rows/row">
				<xsl:element name="tr">
					<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
		    		<!-- for each col in row -->
		    		<xsl:for-each select="col">
						<xsl:choose>
						    <xsl:when test="position() > 1">
						    	<td class="spacing" />
						    </xsl:when>
						</xsl:choose>
		    			<!-- Need to store column position to do the == position() or will get all col attributes (why ... ?) -->
    					<xsl:variable name="columnPosition"><xsl:value-of select="position()"/></xsl:variable>
						<xsl:element name="td">
							<xsl:attribute name="class"><xsl:value-of select="../../../../cols/col[position() = $columnPosition]/@class"/></xsl:attribute>
							<xsl:attribute name="style"><xsl:value-of select="../../../../cols/col[position() = $columnPosition]/@style"/></xsl:attribute>
							
							<!-- Check if column aspect is a link -->
	    					<xsl:variable name="columnAspect"><xsl:value-of select="../../../../cols/col[position() = $columnPosition]/@aspect"/></xsl:variable>

							<xsl:choose>
								<!-- LINK -->
								<xsl:when test="$columnAspect='link'">
									<xsl:element name="a"><xsl:attribute name="href">#_<xsl:value-of select="normalize-space(translate(current(), '.', '_'))"/></xsl:attribute>
										<xsl:apply-templates />
									</xsl:element>
								</xsl:when>
								<!-- LINK -->
							 	<xsl:otherwise>
							 		<xsl:value-of select="." />
							 		<!-- 
									<xsl:apply-templates />
									 -->
							  	</xsl:otherwise>
							</xsl:choose>
							
						</xsl:element>
					</xsl:for-each>
				</xsl:element>
			</xsl:for-each>	
		</tbody>
	</xsl:template>

 	<!-- =========================================================== -->
	<!--                                                             -->
	<!-- HTML STANDARD TAGS -->
	<!--                                                             -->
	<!-- =========================================================== -->

	<!-- =========================================================== -->
	<!-- H1..3 -->
	<!-- =========================================================== -->

	<xsl:template match="h1">
		<h1>
			<xsl:apply-templates />
		</h1>
	</xsl:template>
	<xsl:template match="h2">
		<h2>
			<xsl:apply-templates />
		</h2>
	</xsl:template>
	<xsl:template match="h3">
		<h3>
			<xsl:apply-templates />
		</h3>
	</xsl:template>
	<xsl:template match="h4">
		<h4>
			<xsl:apply-templates />
		</h4>
	</xsl:template>

	<!-- =========================================================== -->
	<!-- P (PARA) -->
	<!-- =========================================================== -->

	<xsl:template match="p">
		<p>
			<xsl:apply-templates />
		</p>
	</xsl:template>
	
	<xsl:template match="b">
		<b><xsl:apply-templates /></b>
	</xsl:template>

	<xsl:template match="img">
		<xsl:element name="img">
			<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
			<xsl:attribute name="alt"><xsl:value-of select="@alt"/></xsl:attribute>
			<xsl:attribute name="src">data:image/png;base64,<xsl:value-of select="axional:get($images, @src)"/></xsl:attribute>
		</xsl:element>
	</xsl:template>

	<xsl:template match="title">
		<div class="title">
			<xsl:apply-templates />
		</div>
	</xsl:template>
		
	<xsl:template match="subtitle">
		<div class="subtitle">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<!-- =========================================================== 
		To clean up the whitespace, we can use the normalize-space() 
		function. It does three things:
		1. It removes all leading spaces.
		2. It removes all trailing spaces.
		3. It replaces any group of consecutive whitespace characters 
		with a single space.
		=========================================================== -->
	 <!--  -->
	<xsl:template match="text()">
    	<xsl:value-of select="normalize-space()"/>
  	</xsl:template>
  
   
	<!-- =========================================================== -->
	<!-- ignore the content of other tags because we processed them elsewhere -->
	<!-- =========================================================== -->
	<xsl:template match="*">
		<!-- do nothing -->
	</xsl:template>

</xsl:stylesheet>